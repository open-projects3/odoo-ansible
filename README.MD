# Odoo install with Ansible

Ansible playbook for installing Odoo 16 on Ubuntu 22.04 with Nginx.
Odoo is installed from git.

Variables are stored in files vars.yml and vars_keys.yml. 

## vars.yml
- odoo_version: **Odoo version for git**
- odoo_dir: **Odoo directory to install**
- odoo_home: **Lunix user name for Odoo**
- wkhtmltopdf_deb_link: **Link for fetching wkhtmltopdf.deb**
- odoo_service: **Name of Odoo systemd service**
- odoo_conf: **Name of Odoo configuration file**
- venv_name: **Name of python virtual environment for running Odoo**


## vars_keys.yml
File encrypted with ansible vault. Has the values for below listed variables.

- ssh_user: **user name to connect over ssh**
- ansible_become_pass: **ssh users password to become sudo**
- odoo_db_user: **PostgreSQL user name for odoo**
- postgresql_user_pass: **PostgreSQL users password for odoo**
- odoo_admin_passwd: **Odoo master password for instance creation, backup, restore and delete**

## Install

1. Configure SSH connection to server with key authenticaton
2. Change IP in inventory.yml
3. Create own vars_keys.yml file with the needed variables
4. Run ansible playbook odoo_play.yml