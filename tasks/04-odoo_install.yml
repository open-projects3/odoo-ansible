---
- name: Get Odoo from git
  become: true
  become_user: "{{ odoo_db_user }}"
  git:
    repo: https://www.github.com/odoo/odoo
    clone: true
    depth: 1
    single_branch: true
    version: "{{ odoo_version }}"
    dest: "{{ odoo_home }}/{{ odoo_dir }}/odoo"

- name: Create Odoo Virtual env
  become: true
  become_user: "{{ odoo_db_user }}"
  pip:
    virtualenv: "{{ odoo_home }}/{{ odoo_dir }}/{{ venv_name }}"
    virtualenv_command: python3 -m venv
    requirements: "{{ odoo_home }}/{{ odoo_dir }}/odoo/requirements.txt"

- name: Create 3rd Party Addons dir
  become: true
  become_user: "{{ odoo_db_user }}"
  file:
    path: "{{ odoo_home }}/{{ odoo_dir }}/thirdpartyaddons"
    state: directory
    mode: 0774

- name: Create Custom Addons dir 
  become: true
  become_user: "{{ odoo_db_user }}"
  file:
    path: "{{ odoo_home }}/{{ odoo_dir }}/customaddons"
    state: directory
    mode: 0774

- name: Add ssh user as group
  become: true
  file:
    path: "{{ odoo_home }}"
    state: directory
    recurse: yes
    group: "{{ ssh_user }}"

- name: Create Log dir
  become: true
  file:
    path: /var/log/odoo
    state: directory
    mode: 0774
    group: "{{ odoo_db_user }}"

- name: Create Log file
  become: true
  file:
    path: /var/log/odoo/odoo.log
    state: touch
    mode: 0644

- name: Get wkhtml2pdf
  become: true
  apt:
    deb: "{{ wkhtmltopdf_deb_link }}"

- name: Install missing dependencies
  become: true
  shell: apt install -f
  
- name: Copy Odoo configuration
  become: true
  copy:
    src: Odoo/odoo.conf
    dest: "/etc/{{ odoo_conf }}"
    mode: 0644

- name: Update Config addons_path 
  become: true
  ini_file:
    path: "/etc/{{ odoo_conf }}"
    section: options
    option: addons_path
    value: "{{ odoo_home }}/{{ odoo_dir }}/odoo/addons,{{ odoo_home }}/{{ odoo_dir }}/thirdpartyaddons,{{ odoo_home }}/{{ odoo_dir }}/customaddons"

- name: Update Config admin_passwd 
  become: true
  ini_file:
    path: "/etc/{{ odoo_conf }}"
    section: options
    option: admin_passwd
    value: "{{ odoo_admin_passwd }}"

- name: Update Config db_user 
  become: true
  ini_file:
    path: "/etc/{{ odoo_conf }}"
    section: options
    option: db_user
    value: "{{ odoo_db_user }}"

- name: Update Config dbfilter 
  become: true
  ini_file:
    path: "/etc/{{ odoo_conf }}"
    section: options
    option: dbfilter
    value: "{{ db_filter }}"

- name: Copy Odoo service
  become: true
  copy:
    src: Odoo/odoo.service
    dest: "/etc/systemd/system/{{ odoo_service }}"
    mode: 0755

- name: Update service ExecStart
  become: true
  ini_file:
    path: "/etc/systemd/system/{{ odoo_service }}"
    section: Service
    option: ExecStart
    value: "{{ odoo_home }}/{{ odoo_dir }}/{{ venv_name }}/bin/python3 {{ odoo_home }}/{{ odoo_dir }}/odoo/odoo-bin -c /etc/{{ odoo_conf }}"

- name: Update service User
  become: true
  ini_file:
    path: "/etc/systemd/system/{{ odoo_service }}"
    section: Service
    option: User
    value: "{{ odoo_db_user }}"
  
- name: Enabling and starting Odoo
  become: true
  systemd:
    name: "{{ odoo_service }}"
    state: started
    enabled: true

- name: Restart Odoo
  become: true
  systemd:
    name: "{{ odoo_service }}"
    state: restarted
  

...